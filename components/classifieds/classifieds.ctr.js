(function(){

    "use strict";

    angular
        //reference the module created
        .module('ngClassified')
        //create controller
        .controller('classifiedCtrl', function($scope, $http, classifiedsFactory, $mdSidenav, $mdToast, $mdDialog, $state){ //dependencies

            var vm = this;

            vm.categories;
            vm.classified;
            vm.classifieds;
            vm.closeSidebar = closeSidebar;
            vm.deleteClassified = deleteClassified;
            vm.editClassified = editClassified;
            vm.editing;
            vm.openSidebar = openSidebar;
            vm.saveClassified = saveClassified;
            vm.saveEdit = saveEdit;

            // call factory
            classifiedsFactory.getClassifieds().then(function(classifieds){
                vm.classifieds = classifieds.data;
                vm.categories = getCategories(vm.classifieds);
            });

            $scope.$on('newClassifieds', function(event, classified){
              classified.id = vm.classifieds.length + 1;
              vm.classifieds.push(classified);
              showToast('Classified saved');
            });

            var contact = {
              name: "Raphael Denys",
              phone: '(21) 1111-1111',
              email: 'rdenys@gmail.com'
            }

            //open sidenav
            function openSidebar(){
                $state.go('classifieds.new');
            }

            //close sidenav
            function closeSidebar(){
                $mdSidenav('left').close();
            }

            function saveClassified(classified){
              if(classified){
                classified.contact = contact;
                vm.classifieds.push(classified);
                vm.classified = {};
                closeSidebar();
                showToast('Classified created');
              }
            }

            function editClassified(classified){
              $state.go('classifieds.edit', {
                id: classified.id,
                classified: classified
              });
            }

            function saveEdit(){
              vm.editing = false;
              vm.classified = {};
              closeSidebar();
              showToast('Edit save');
            }

            function deleteClassified(event, classified){
              var confirm = $mdDialog.confirm()
                                .title('Are you sure ' + classified.title + '?')
                                .ok('yes')
                                .cancel('no')
                                .targetEvent(event);

              $mdDialog.show(confirm).then(function(){
                var index = vm.classifieds.indexOf(classified);
                vm.classifieds.splice(index, 1);
              }, function(){

              });
            }

            function showToast(message){
              $mdToast.show(
                $mdToast.simple()
                  .content(message)
                  .position('top, right')
                  .hideDelay(3000)
              );
            }

            function getCategories(classifieds){
              var categories = [];
              angular.forEach(classifieds, function(item){
                angular.forEach(item.categories, function(category){
                  categories.push(category);
                })
              });
              return _.uniq(categories);
            }

        });

})();
