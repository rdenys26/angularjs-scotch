(function(){
    "use strict";

    angular
        .module('ngClassified')
        .controller('newClassifiedsCtrl', function($scope, $mdSidenav, $mdDialog, classifiedsFactory, $timeout, $state){
            
            var vm = this;
                vm.closeSidebar = closeSidebar;
                vm.saveClassified = saveClassified;

            $timeout(function(){
                $mdSidenav('left').open();
            });

            $scope.$watch('vm.sidenavOpen', function(sidenav){
                if(sidenav === false){
                    $mdSidenav('left')
                        .close()
                        .then(function(){
                            $state.go('classifieds');
                        });
                }
            });

            function closeSidebar(){
                vm.sidenavOpen = false;
            }

            function saveClassified(classified){
                if(classified){

                    classified.contact = {
                      name: "Raphael Denys",
                      phone: '(21) 1111-1111',
                      email: 'rdenys@gmail.com'
                    }

                    $scope.$emit('newClassifieds', classified);
                    vm.sidenavOpen = false;

                }
            }


        });
})()